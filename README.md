CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Usage


INTRODUCTION
------------

The easiest solution for disabling Google Chrome auto-fill, auto-complete functions.

See the https://github.com/terrylinooo/jquery.disableAutoFill repo for library details.


INSTALLATION
------------

Install as usual Drupal module.


USAGE
-----

This module provides the `jquery.disableAutoFill` library with the script which
initiate the library for specifically selected with `js-disable-auto-fill`
class.

Steps to do:

1. Attach a library:
   ```php
   $form['#attached']['library'][] = array('jquery_disable_auto_fill', 'init');
   ```

2. Select a scope of disabled form fields:
   ```php
   // To disable autofill for entire form.
   $form['#attributes']['class'][] = JQUERY_DISABLE_AUTO_FILL_CLASS_NAME;
   ```
   OR
   ```php
   // To disable autofill for specific form element.
   $form['specific_form_element_name']['#attributes']['class'][] = JQUERY_DISABLE_AUTO_FILL_CLASS_NAME;
   ```
