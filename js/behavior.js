(function($, Drupal) {
  Drupal.behaviors.jquery_disable_auto_fill = {
    "attach": function(context, settings) {
      $(".js-disable-auto-fill").once("jquery_disable_auto_fill_initiated", function(a,b) {
        $(this).disableAutoFill();
      });
    }
  };
})(jQuery, Drupal);
